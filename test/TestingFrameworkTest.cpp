#include <HEM/TestingFramework.hpp>

using namespace fakeit;

class ITest
{
public:
    virtual int getInt() = 0;
};

TEST_CASE("Testing Framework Test"){
    constexpr int someIntValue = 123456;

    Mock<ITest> testMock;
    When(Method(testMock, getInt)).Return(someIntValue);

    int readValue = testMock.get().getInt();

    REQUIRE(readValue == someIntValue);
}

#if __cplusplus > 201703L

TEST_CASE("Span operators test")
{
  std::array lhs = {1, 2, 3, 4};
  std::array rhs = { 1, 2, 3, 4};
  std::span lhsSpan = lhs;
  std::span rhsSpan = rhs;

  REQUIRE(lhsSpan == rhsSpan);
}

#endif