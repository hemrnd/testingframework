# HEM RnD TestingFramework
This small package integrates [Catch2](https://github.com/catchorg/Catch2) and 
[FakeIt](https://github.com/eranpeer/FakeIt) to be used easier in other packages. 
It uses [CPM.cmake](https://github.com/cpm-cmake/CPM.cmake) to get Catch2 and FakeIt libraries (our forks) 
and integrate them into easy to use CPM ready package. It also includes cmake functions add_test_suite 
and add_test_suites to easier add tests in cmake files.

## Example use in some project:
`CMakeLists.txt` in test directory:
```cmake
CPMAddPackage(
        NAME TestingFramework
        GIT_REPOSITORY https://bitbucket.org/hemrnd/TestingFramework.git
        VERSION 0.3.0)

add_test_suites(
        TESTS SomeTest
        LINK_LIBRARIES ${PACKAGE_NAME})
```
`SomeTest.cpp`:
```c++
#include <HEM/TestingFramework.hpp>

using namespace fakeit;

class ITest
{
public:
    virtual int getInt() = 0;
};

TEST_CASE("Testing Framework Test"){
    constexpr int someIntValue = 123456;

    Mock<ITest> testMock;
    When(Method(testMock, getInt)).Return(someIntValue);

    int readValue = testMock.get().getInt();

    REQUIRE(readValue == someIntValue);
}
```
