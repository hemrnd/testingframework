include(Catch)
include(CTest)

add_custom_target(build_tests)

function(add_test_suite)
    cmake_parse_arguments(
            TEST_SUITE
            ""
            "TARGET"
            "SOURCES;LINK_LIBRARIES"
            ${ARGN}
    )
    add_executable(${TEST_SUITE_TARGET})
    target_sources(${TEST_SUITE_TARGET} PRIVATE ${TEST_SUITE_SOURCES})
    target_link_libraries(${TEST_SUITE_TARGET} PUBLIC ${TEST_SUITE_LINK_LIBRARIES} TestingFramework)
    foreach (library_target ${TEST_SUITE_LINK_LIBRARIES})
        get_target_property(library_type ${library_target} TYPE)
        if (NOT (${library_type} STREQUAL INTERFACE_LIBRARY))
            get_target_property(include_directories ${library_target} INCLUDE_DIRECTORIES)
            target_include_directories(${TEST_SUITE_TARGET} PUBLIC ${include_directories})
        endif ()
    endforeach ()
    catch_discover_tests(${TEST_SUITE_TARGET})
    add_dependencies(build_tests ${TEST_SUITE_TARGET})
endfunction()

function(add_test_suites)
    cmake_parse_arguments(
            TEST_SUITE
            ""
            "TEST_PATH"
            "TESTS;LINK_LIBRARIES"
            ${ARGN}
    )

    if ("${TEST_SUITE_TEST_PATH}" STREQUAL "")
        set(TEST_DIR ${CMAKE_CURRENT_LIST_DIR})
    else ()
        set(TEST_DIR ${TEST_SUITE_TEST_PATH})
    endif ()

    foreach (TEST ${TEST_SUITE_TESTS})
        add_test_suite(
                TARGET ${TEST}
                SOURCES ${TEST_DIR}/${TEST}.cpp
                LINK_LIBRARIES ${TEST_SUITE_LINK_LIBRARIES}
        )
    endforeach ()
endfunction()