#pragma once

#if defined __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#endif

#include "HEM/SpanOperators.hpp"

#include <catch2/catch.hpp>
#include <fakeit.hpp>

#if defined __GNUC__
#pragma GCC diagnostic pop
#endif
