#pragma once

#if __cplusplus > 201703L

#include <span>

template<typename T, std::size_t ExtentL, std::size_t ExtentR>
bool operator== (const std::span<T, ExtentL>& lhs, const std::span<T, ExtentR>& rhs)
{
    if (lhs.size() != rhs.size())
        return false;
    for (size_t i = 0; i < rhs.size(); ++i)
        if (rhs[i] != lhs[i])
            return false;
    return true;
}

template<typename T, std::size_t ExtentL, std::size_t ExtentR>
bool operator!= (const std::span<T, ExtentL>& lhs, const std::span<T, ExtentR>& rhs)
{
    return !(lhs == rhs);
}

#endif