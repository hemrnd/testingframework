#!/usr/bin/env bash

echo "Build running"
set -e
mkdir -p build
cmake -B ./build -DCMAKE_BUILD_TYPE=Debug -DTESTING_FRAMEWORK_BUILD_TESTING=ON .
cmake --build ./build -t build_tests |& tee build/console.log
cmake --build ./build -t test